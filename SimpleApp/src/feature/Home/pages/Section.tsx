import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  Image,
  View,
  Button,
  SectionList,
  TouchableHighlight,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Modal,
  Alert,
} from "react-native";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  item: {
    padding: 20,
    marginVertical: 8,
  },
  header: {
    fontSize: 32,
    backgroundColor: "#fff",
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(0,0,0,0.3)",
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
});
const data = [
  {
    title: "Text",
    data: ["text1", "text2", "text3", "text4", "text5"],
  },
  {
    title: "Buttons",
    data: [
      "Button",
      "TouchableHighlight",
      "TouchableOpacity",
      "TouchableWithoutFeedback",
    ],
  },
];

export default function Section() {
  const [modalVisible, setModalVisible] = useState(false);
  return (
    <View style={styles.container}>
      <SectionList
        sections={data}
        renderSectionHeader={({ section: { title } }) => (
          <Text style={styles.header}>{title}</Text>
        )}
        renderItem={({ item, index }) => {
          if (item.startsWith("text")) {
            return (
              <View style={styles.item}>
                <Text
                  style={{
                    fontSize: 12 + index * 2.5,
                    textAlign: "center",
                  }}
                >
                  {item}
                </Text>
              </View>
            );
          }
          if (item === "Button") {
            return (
              <View style={styles.item}>
                <Button title={item} onPress={() => console.log(item)} />
              </View>
            );
          }
          if (item === "TouchableHighlight") {
            return (
              <View style={styles.item}>
                <TouchableHighlight onPress={() => console.log(item)}>
                  <Text style={{ textAlign: "center" }}>{item}</Text>
                </TouchableHighlight>
              </View>
            );
          }
          if (item === "TouchableOpacity") {
            return (
              <View style={styles.item}>
                <TouchableOpacity onPress={() => console.log(item)}>
                  <Text style={{ textAlign: "center" }}>{item}</Text>
                </TouchableOpacity>
              </View>
            );
          }
          if (item === "TouchableWithoutFeedback") {
            return (
              <View style={styles.item}>
                <TouchableWithoutFeedback onPress={() => setModalVisible(true)}>
                  <Text style={{ textAlign: "center" }}>{item}</Text>
                </TouchableWithoutFeedback>
              </View>
            );
          }
          return null;
        }}
      />
      <Modal
        animationType="slide"
        visible={modalVisible}
        transparent
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text>This is a modal</Text>
            <TouchableHighlight onPress={() => setModalVisible(false)}>
              <Image
                source={{
                  uri:
                    "https://lh3.googleusercontent.com/proxy/gCHdpe4s7X0ndd51ScB6FXET8TX8PZ6_HIXyZtH0z6UVGblI0-TXIgnu5vUOd5sAgwB5lxDMVvqiDdHkY18fk54qEsttED3wFRji",
                }}
                style={{ width: 200, height: 200 }}
              />
            </TouchableHighlight>
          </View>
        </View>
      </Modal>
    </View>
  );
}
