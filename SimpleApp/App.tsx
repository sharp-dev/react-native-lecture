import 'react-native-gesture-handler';
import React from 'react';
import Router from './src/shared/router';


export default function App() {
  return (
    <Router />
  );
}