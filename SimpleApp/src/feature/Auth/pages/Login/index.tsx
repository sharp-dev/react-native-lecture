import React, { useCallback, useState, useEffect } from "react";
import { StatusBar } from "expo-status-bar";
import {
  StyleSheet,
  Text,
  Image,
  View,
  KeyboardAvoidingView,
  ScrollView,
  TextInput,
  Button,
} from "react-native";
import { Formik, FormikValues } from "formik";
import { useSafeAreaInsets } from "react-native-safe-area-context";
import * as Yup from "yup";
import { useNavigation } from "@react-navigation/native";
import { StackNavigationProp } from "@react-navigation/stack";
import { StackRoute } from "../../../../shared/router/StackRoute";
import * as LocalAuthentication from "expo-local-authentication";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  keyboard: {
    flex: 1,
  },
  scrollContent: {
    flexGrow: 1,
    justifyContent: "space-between",
    flexDirection: "column",
  },
  logo: {
    alignSelf: "center",
    resizeMode: "contain",
    width: 200,
    height: 200,
    marginVertical: 16,
  },
  label: { fontSize: 18, marginVertical: 8 },
  input: {
    fontSize: 22,
    marginVertical: 8,
    borderWidth: 1,
    padding: 8,
    borderRadius: 8,
  },
  form: { flex: 2, justifyContent: "center", marginHorizontal: 16 },
  copyright: { alignSelf: "center" },
  errorText: {
    color: "red",
  },
});

interface FormValues extends FormikValues {
  email?: string;
  password?: string;
}

const schema = Yup.object().shape({
  email: Yup.string().email("Invalid email").required("Required"),
  password: Yup.string()
    .min(6, "Password is too short, at least 6")
    .max(16, "Password is too long, no more than 16")
    .required("Required"),
});

const LoginPage = () => {
  const insets = useSafeAreaInsets();
  const navigation = useNavigation<StackNavigationProp<StackRoute>>();
  const [hasLocalAuth, setHasLocaAuth] = useState(false);

  const onSubmit = useCallback((values: FormValues) => {
    console.log(values);
    navigation.push("Home");
  }, []);

  useEffect(() => {
    const checkLocalAuth = async () => {
      setHasLocaAuth(await LocalAuthentication.hasHardwareAsync());
    };
    checkLocalAuth();
  }, [])
  return (
    <View style={[styles.container, { paddingBottom: insets.bottom }]}>
      <StatusBar style="auto" />
      <KeyboardAvoidingView style={styles.keyboard} behavior="padding" enabled>
        <ScrollView contentContainerStyle={styles.scrollContent}>
          <Image
            style={styles.logo}
            source={{
              uri:
                "https://www.codeplusinfo.com/wp-content/uploads/2020/02/react-native-logo-e1581157043920.png",
            }}
          />
          <Formik<FormValues>
            initialValues={{ email: "", password: "" }}
            onSubmit={onSubmit}
            validationSchema={schema}
          >
            {({
              handleChange,
              handleBlur,
              handleSubmit,
              errors,
              touched,
              values,
            }) => (
              <View style={styles.form}>
                <Text style={styles.label}>Email</Text>
                <TextInput
                  onChangeText={handleChange("email")}
                  onBlur={handleBlur("email")}
                  value={values.email}
                  style={styles.input}
                />
                {errors.email && touched.email && (
                  <View>
                    <Text style={styles.errorText}>{errors.email}</Text>
                  </View>
                )}
                <Text style={styles.label}>Password</Text>
                <TextInput
                  onChangeText={handleChange("password")}
                  onBlur={handleBlur("password")}
                  value={values.password}
                  secureTextEntry
                  style={styles.input}
                />
                {errors.password && touched.password && (
                  <View>
                    <Text style={styles.errorText}>{errors.password}</Text>
                  </View>
                )}
                <Button onPress={() => handleSubmit()} title="Submit" />
              </View>
            )}
          </Formik>
          {hasLocalAuth && <Button title="Local auth" onPress={async () => {
            if (await LocalAuthentication.isEnrolledAsync()) {
              const res = await LocalAuthentication.authenticateAsync();
              if (res.success) {
                navigation.push("Home");
              }
            }
          }} />}
          <Text style={styles.copyright}>(C) koWalskiWar 2020</Text>
        </ScrollView>
      </KeyboardAvoidingView>
    </View>
  );
};

export default LoginPage;
