import React, { useState } from "react";
import { createMaterialTopTabNavigator } from "@react-navigation/material-top-tabs";
import Section from "./Section";
import Flat from "./Flat";
import CameraPage from "./Camera";
import LocationPage from "./Location";
import MapPage from "./Map";



const Tab = createMaterialTopTabNavigator();


const HomePage = () => {
  return (
    <Tab.Navigator tabBarOptions={{labelStyle: {fontSize: 10}}}>
      <Tab.Screen name="Section" component={Section} />
      <Tab.Screen name="FlatList" component={Flat} />
      <Tab.Screen name="Camera" component={CameraPage} />
      <Tab.Screen name="Location" component={LocationPage} />
      <Tab.Screen name="Map" component={MapPage} />
    </Tab.Navigator>
  );
};

export default HomePage;
