import React, { useState } from "react";
import { StyleSheet, Text, View, RefreshControl, FlatList } from "react-native";

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "#fff",
    },
    item: {
      padding: 20,
      marginVertical: 8,
    },
  });

export default function Flat() {
  const [isRefreshing, setRefreshing] = useState(false);
  const [flat, setFlat] = useState([{ title: "item" }]);

  return (
    <FlatList
      data={flat}
      refreshControl={
        <RefreshControl
          refreshing={isRefreshing}
          onRefresh={() => {
            setRefreshing(true);
            setTimeout(() => {
              setFlat([...flat, ...flat]);
              setRefreshing(false);
            }, 2000);
          }}
        />
      }
      keyExtractor={(item, index) => index.toString()}
      // onEndReached={() => {
      //   setRefreshing(true);
      //   setTimeout(() => {
      //     setFlat([...flat, ...flat]);
      //     setRefreshing(false);
      //   }, 2000);
      // }}
      renderItem={({ item }) => {
        return (
          <View style={styles.item}>
            <Text style={{ textAlign: "center" }}>{item.title}</Text>
          </View>
        );
      }}
    />
  );
}
